// Import the page-mod API
var pageMod = require("sdk/page-mod");

var data = require("sdk/self").data;

// Create a page-mod
pageMod.PageMod({
  include: "https://github.com/*",
  contentScriptFile: data.url("jquery.js"),
  contentScript: '$(".repository-meta-content").find("a").attr("target","_blank");'
});
