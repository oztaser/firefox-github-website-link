#Github Website Link Target
Repository sayfasının üstünde bulunan website linkinin neden yeni bir pencerede açılmadığına anlam veremiyor ve buna sinir oluyordum.
Bir çözümde bulamayınca ilk defa bir firefox eklentisi geliştirme işine atıldım ve gayet basit bir şekilde sorunumu çözdüm. 

Bilemiyorum belki çok amatörce ve kötü bir yöntem olabilir ama elimden bu geldi :) Tekrarlıyorum ilk firefox uygulamam varsa bir sıkıntı yanlış düzeltmeyi, öğrenmeyi çok isterim.

Depodaki xpi uzantılı dosyayı Add-ons Manager altında Install Add-on From File kısmından yükleyebilirsiniz.

Addons Mozilla adresi: https://addons.mozilla.org/en-US/firefox/addon/github-website-link-target/

[![image](http://i.hizliresim.com/kLBngW.png)]

###Geliştirme
Firefox için eklenti geliştirmek için öncelikle nodejs ve npm kurmalısınız:
> sudo apt-get install nodejs

> sudo apt-get install npm

Ardından Addon SDK kurmak için şu komutu kullanmalısınız:
> sudo npm install jpm --global

Javascript geliştirmeleri yaptıktan sonra şu komut ile eklentiyi test edebilirsiniz:
> jpm run -b /usr/bin/iceweasel 

Baktınız eklenti istediğiniz gibi çalışıyor alttaki komut ile derleyip kurulum dosyası haline getirebilirsiniz.
> jpm xpi 

Javascript bilmediğimden benden bu kadar :) umuyorum saçma bir çözüm değildir :?

###Lisans
GPLv3
